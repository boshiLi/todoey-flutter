import 'dart:collection';

import 'package:flutter/foundation.dart';

class Task {
  final String name;
  bool isDone;

  Task({this.name, this.isDone = false});

  void toggleDone() {
    this.isDone = !isDone;
  }
}

class TaskData extends ChangeNotifier {
  List<Task> _tasks = [];
  UnmodifiableListView<Task> get tasks => UnmodifiableListView(_tasks);

  void addTask(String newTask) {
    if (newTask != null && newTask.isNotEmpty) {
      this._tasks.add(Task(name: newTask));
    }
    notifyListeners();
  }

  void updateTask({Task task, int index}) {
    this._tasks[index] = task;
    notifyListeners();
  }
}
