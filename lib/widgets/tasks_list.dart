import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoey_flutter/model/task.dart';
import 'package:todoey_flutter/widgets/task_tile.dart';

class TaskListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<TaskData>(
      builder: (context, taskData, child) {
        final List<Task> tasks = taskData.tasks;
        return ListView.builder(
          itemCount: tasks.length,
          itemBuilder: (context, index) {
            return TaskListTile(
              task: tasks[index],
              onChanged: (v) {
                tasks[index].toggleDone();
                taskData.updateTask(task: tasks[index], index: index);
              },
            );
          },
        );
      },
    );
  }
}
