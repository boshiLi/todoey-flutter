import 'package:flutter/material.dart';
import 'package:todoey_flutter/model/task.dart';

class TaskListTile extends StatelessWidget {
  final Task task;
  final ValueChanged<bool> onChanged;

  TaskListTile({this.task, @required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text(
        this.task.name,
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w600,
          color: Colors.black,
          decoration: this.task.isDone ? TextDecoration.lineThrough : null,
        ),
      ),
      trailing: Checkbox(
        value: this.task.isDone,
        onChanged: onChanged,
      ),
    );
  }
}
