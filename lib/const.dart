import 'package:flutter/material.dart';

const kCardRadius = BorderRadius.only(
  topLeft: Radius.circular(20),
  topRight: Radius.circular(20),
);
