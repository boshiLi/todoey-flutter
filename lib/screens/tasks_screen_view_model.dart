import 'package:todoey_flutter/model/task.dart';

class TaskScreenViewModel {
  List<Task> tasks = [];

  void addTask(String name) {
    if (name.isNotEmpty) {
      this.tasks.add(Task(name: name));
    }
  }

  void toggleTaskDone({int index, Task task}) {
    this.tasks[index].toggleDone();
  }
}
